'''
Problem 1
Multiples of 3 and 5
=======================
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
Find the sum of all the multiples of 3 or 5 below 1000.
'''
NUM,SUM = 0,0	
while NUM < 16:
	if NUM % 3 == 0 or NUM % 5 == 0:
		print NUM,
		SUM = SUM + NUM
	NUM += 1
print "\nSum of 3 of 5 multiple till 10 is:",SUM
