# 145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.

# Find the sum of all numbers which are equal to the sum of the factorial of their digits.

# Note: as 1! = 1 and 2! = 2 are not sums they are not included.

def factorial(L):
    """Get the factorials"""
    sum_factorial = 0
    for i in L:
        fact = 1
        for j in range(1, (int(i) + 1)):
            fact = fact * j
        sum_factorial = sum_factorial + fact
    return sum_factorial
 
def main():
    """Main Program"""
    L = []
    LN = []
    for n in range(1, 1000001):
        L = list(str(n))
        x = factorial(L)
        if x == n:
            LN.append(n)
    y = sum(LN)
    print "LN = ", LN
    print "Answer = ", y - 3
    # subract 3 for 1! and 2!
 
if __name__ == '__main__':
    main() 